﻿using System;
using System.Collections.Generic;

namespace TraverseSpirally
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] jagged1 = new int[][]
            {
                new int[] {1, 2, 3},
                new int[] {4, 5, 6},
                new int[] {7, 8, 9}
            };

            int[][] jagged2 = new int[][]
            {
                new int[] {1, 2, 3, 4},
                new int[] {5, 6, 7, 8},
                new int[] {9, 10, 11, 12}
            };

            int[][] jagged3 = new int[][]
            {
                new int[] {1, 2}
            };
            List<int> n = SpiralOrder(jagged2);
            for (int i = 0; i < n.Count; i++)
            {
                System.Console.Write(n[i] + " ");
            }
            System.Console.WriteLine();
        }

        public static List<int> SpiralOrder(int[][] matrix) {
            List<int> spiralList = new List<int>();
            int n = matrix.Length;
            int m = matrix[0].Length;
            int k = 0;
            int l = 0;

            int lastRow = n - 1;
            int lastColumn = m - 1;

            while(k <= lastRow & l <= lastColumn){
                for (int i = l; i <= lastColumn; i++)
                {
                    spiralList.Add(matrix[k][i]);
                }
                k++;
                for (int i = k; i <= lastRow; i++)
                {
                    spiralList.Add(matrix[i][lastColumn]);
                }
                lastColumn--;

                if(k <= lastRow)
                {
                    for (int i = lastColumn; i >= l; i--)
                    {
                        spiralList.Add(matrix[lastRow][i]);
                    }
                    lastRow--;
                }

                if(l <= lastColumn)
                {
                    for (int i = lastRow; i >= k; i--)
                    {
                        spiralList.Add(matrix[i][l]);
                    }
                    l++;
                }
            }
            return spiralList;
        }
    }
}
